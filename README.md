# qutr.gitlab.io

Website content for the QUT Robotics Wiki

## Installing and testing locally
Before starting, ensure you have `Node.js` and `npm` installed. This should work across all platforms supported by Node.js.
``` bash
# Dowload the repository
git clone https://gitlab.com/qutr/qutr.gitlab.io.git
cd qutr.gitlab.io

# Install dependencies
npm install
npm run install

# Build the static files (optional)
npm run build

# Run the local server
npm run serve

# Browse to http://localhost:4000 to view the website
# Changes made to the source should cause the site to dynamically update
```

## Reminders

- Need to add in an example page for general tools
  - images
  - equations
  - linking internally/externally
  - Link out to: https://toolchain.gitbook.com/
-  More things to add to book.json?
- Notes on how to organise content within the site
- Links back to the info repository for other information like licensing
- Notes on wiki layout to make it easy to keep track of content and images
  - Folder structure
  - Special pages
    - CPAGE.md
    - GLOSSARY.md
- Information about the group
  - Adding repositories
  - Visibility settings
- Using anchors (#) in internal links
