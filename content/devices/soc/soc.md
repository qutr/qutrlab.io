{% extends "/CPAGE.md" %}

{% set maintainers = [""] %}

{% block pageContent %}

# Socket on Chip (SoC)
For use in a complex system, it is critical that the on-board computer is compatible across all subsystems. Most importantly, it is critical to work out the load that any systems that will use the on-board computer. For this reason, it is highly suggested that a trade study, or at least an educated consideration, is performed.

It is highly suggested that you pick an on-board computer that is capable of installing ROS (i.e. it can run Ubuntu). For this reason, a popular pick may be a Raspberry Pi 2/3 (while you can install ROS on the default OS, NOOBS, it is rather tricky, and compatibility is rough when compared to the Ubuntu method). With this said, there are still many other options, such as the Odroid U3 (or similar if this is unavailable), as well as many other Socket on Chip (SoC) ARM-based processors.

## Comparison of Common On-Board Computers
| Name                | Weight | CPU       | Memory | Hard-Drive | Av. Power Draw |
|:------------------- | ------:| ---------:| ------:|:----------:| --------------:|
| Raspberry Pi Zero W | 9g     | 1x 1.0GHz |  0.5GB | SD Card    | 180mA          |
| Raspberry Pi 2      | 45g    | 4x 0.9GHz |  1.0GB | SD Card    | 800mA          |
| Raspberry Pi 3      | 45g    | 4x 1.0GHz |  1.0GB | SD Card    | 800mA          |
| Odroid U3           | 48g    | 4x 1.7GHz |  2.0GB | eMMC Chip  | 800mA          |
| Odroid XU4          | 45g    | 8x 2.0GHz |  1.0GB | eMMC Chip  | 1000mA         |

Note that while having multiple cores is a huge improvement in performance, it does note correlate to a direct increase in performance (for example a quad-core computer may only run 2.5x faster than a single-core computer, for most tasks). Performance-wise, software needs to be specifically programmed to make use of additional processing cores.

## Operating Systems
Operating systems for on-board computers come in all types of flavors, depending on what the task of the on-board computer will be, how reliable it needs to be, and how much development has been put into the device in general.

For most on-board systems (such as cameras modules and autopilots), we need a high reliability on the when the software is run, and when we can expect an even to happen. For other systems, such as image processing or high-level, navigation, we can (usually) allow for some delays in the response of the system (e.g. it may not matter if there is an extra 10ms delay between images that are being live processed).

Most likely, the on-board system will be taking up one of the less critical roles in the system, and as such, if there are slight delays, or we miss a frame, etc., there won't be a huge loss in the overall performance of the system. It is also a lot more likely that it is much more desirable for the system to be dynamic (e.g. can use high-level programming languages, compile software dynamically, solve open-ended problems).

For these reasons, it is likely that the best case would be to match the on-board OS with the OS that is highly compatible with the rest of the system (i.e. compatible with the GCS), or if using ROS, to run a "slimmed down" version of Ubuntu.

### Real-Time OS vs. Soft Real-Time OS vs. Everything Else
As mentioned above, there are different levels that an operating system can run at in terms of how predictable and reliable the program execution is. The primary consensus with the different operating systems is that for most applications, those that are not real-time critical, a typical modern operating system will do well enough to maintain decent timing for applications on a shared device.

If nothing else, take away this: If you are going to use a classic method of timing i.e. ``sleep(100)`` or ``delay(20)``, your application probably doesn't warrant the use of a real-time system.

The benefit of a real-time system is that it allows for software to be run reliably, and for a Real-Time OS (RTOS) it means that the OS will know exactly how long it can give to different programs that may be running without causing another to miss a specified target or update rate. The downside to running an environment like this is that there is very little that can be dynamic about it, or rather, the entire OS needs to be compiled in as a single "program". This means that we cannot develop or compile on-the-fly on these types of systems.

The next best option is a soft RTOS, that is, an operating system that is designed to do it's absolute best to meet deadlines, and has inbuilt methods for dealing with missed events or skewed update rates. While this type of OS does its best to meet the demands, there is no absolute guarantee that programs will be reliable. This type of system does have the benefit of being dynamic, that is, it can compile and run programs, use system modules, etc., but the more dynamic it is, the less reliable it will become.

Going back to a regular OS, such as those running the regular Linux kernel, we are in the territory of the OS trying its best to achieve the code execution we are asking, while the system can be completely dynamic. For most applications, as long as they are not running at extremely fast (<5ms) update rates, a regular OS will provide expected performance; most of the time. The problem of having a dynamic system run with optimal performance is one that can be "solved" to a certain degree, usually by simply utilizing faster hardware components (CPU, hard-drives, etc.), and by limiting the amount of programs that are run to give the required software the best chance of being run on time.

In summary, if you want the luxury of having a dynamic system, it inherently cannot be a real-time system.

## Other Considerations
A major issue with most of SoC computers is the use of SD cards as file systems. This can cause a fair amount of issues as SD cards have quite a long read/write time. If possible such devices should be avoided in favor of systems that implement in-build Flash Memory or use eMMC chips (a sort of high-speed SD card).


{% endblock %}
