{% extends "/CPAGE.md" %}

{% block pageContent %}

## Reference Documents
Depending on your autopilot, you may be expected to setup motors, servos, etc., to in specific locations. Additionally, it is critical that the connections to the flight controller are as expected by the flight software.

#### PX4
A list of supported platforms and references can be found at on the [PX4 Developer Wiki](https://dev.px4.io/en/airframes/airframe_reference.)

#### Robin
At time of writing, the _robin_ flight controller only supports the QUAD_X configuration, and matches the documentation in the PX4 Airframe Reference. Please double-check the [robin repository](https://github.com/qutas/robin/) for compatibility before flight.

## Fixed-Wing

#### Comparison of Common Styles

#### Propulsion

## Multirotor

#### Comparison of Common Styles

#### Propulsion

## Helicopters

#### Comparison of Common Styles

#### Propulsion

{% endblock %}
