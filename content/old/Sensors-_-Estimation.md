{% extends "/CPAGE.md" %}

{% block pageContent %}

## Common Sensors

### IMU
#### Sensor Types
#### Common Interfaces
#### Calibration Techniques

### Gyroscopes
#### Sensor Types
#### Common Interfaces
#### Calibration Techniques

### Magnetometers
#### Sensor Types
#### Common Interfaces
#### Calibration Techniques

### Cameras
#### Sensor Types
- Rolling Shutter
- Global Shutter

#### Common Interfaces
- UVC
- Ethernet

#### Calibration Techniques

## Estimation Techniques
### AHRS
#### EKF
### Relative Pose Estimation
#### SolvePnP

{% endblock %}
