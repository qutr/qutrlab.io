{% extends "/CPAGE.md" %}

{% block pageContent %}

## UAVTAQ Emulator

## Robin Simulations

## PX4 Simulations
For information on obtaining the PX4 Firmware (and simulation tools), see the [PX4 Developer Wiki](https://dev.px4.io/en/setup/dev_env_linux.html).

#### Gazebo

#### jMAVSim
For information on running the jMAVSim simulator, see the [PX4 Developer Wiki](https://dev.px4.io/en/simulation/jmavsim.html).

For information on connecting MAVROS to jMAVSim, see the [PX4 Developer Wiki](https://dev.px4.io/en/simulation/ros_interface.html)

For an example of an automated navigation Node in ROS, see the [PX4 Developer Wiki](https://dev.px4.io/en/ros/mavros_offboard.html)

**Sending Position Goals with an Existing Navigation Node**

Assuming you have a ROS Node that is already capable of sending out `geometry_msgs/PoseStamped` message with a position setpoint for the UAV to follow, with the intention of controlling it's position and orientation in XYZ space, you can do the following:

1. First off, launch jMAVSim and MAVROS.
2. ake sure the navigation node is outputting to the MAVROS position setpoint topic (by default `/mavros/setpoint_position/local`). If you require feedback for your navigation Node, you can subscribe to the MAVROS current position topic (by default `/mavros/local_position/pose`).
3. To tell the UAV to listen to the setpoint command, you must request OFFBOARD mode:
```sh
rosrun mavros mavsys mode -c OFFBOARD
```
4. Arm the UAV:
```sh
rosrun mavros mavsafety arm
```
5. The UAV should now be tracking the current setpoint.

{% endblock %}
