{% extends "/CPAGE.md" %}

{% block pageContent %}

This page contains a listing of common and useful terminology that is used throughout this wiki.

#### On-Board
In programming, typically in reference to an operation being performed on the same computer chip.
In design, typically in reference to an object being on the airborne platform.

#### Off-Board
In programming, typically in reference to an operation being performed on some other computer chip.
In design, typically in reference to an object being on the ground or on a different airborne platform.

#### Sosftware-In-The-Loop (SITL)
This is a method of running a simulation of a system completely in software. It typically involves setting up a full environment to simulate all of the hardware involved, as well as all of the software that is running.

#### Hardware-In-The-Loop (HITL)
This is a method of running a simulation of a system while implementing some or all of the hardware that the system will actually use. This method is usually a better indication of the performance of a system, and allows for a more involved debugging process. It typically involves running the software on the involved hardware in a simulation mode, and then providing certain information (such as the state of an aircraft or images from a camera) from a simulator or more powerful computer.

{% endblock %}
