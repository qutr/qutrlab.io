{% extends "/CPAGE.md" %}

{% block pageContent %}

# Old Resources
This is a small landing page for old QUTAS resources before they have been merged into the full listings
<!--
## Pages left to integrate
* [**Common Terminology**](Common-Terminology.md)
* [**Systems Engineering**](Systems-Engineering.md)
* [**General Computing**](General-Computing.md)
  * [Linux in a Nutshell](General-Computing#linux-in-a-nutshell.md)
  * [GUI vs. CLI](General-Computing#gui-vs-cli.md)
  * [Preparing a Work Environment](General-Computing#preparing-a-work-environment.md)
  * [Serial Access](General-Computing#serial-access.md)
  * [Networking in Linux](General-Computing#networking-in-linux.md)
  * [Remote Access](General-Computing#remote-access.md)
  * [Time](General-Computing#time.md)
  * [Changing Software Priority](General-Computing#changing-software-priority.md)
  * [Accessing USB Devices](General-Computing#accessing-usb-devices.md)
* [**On-Board Computers**](On-Board-Computers.md)
  * [Comparison of On-Board Computers](On-Board-Computers#comparison-of-common-on-board-computers.md)
  * [Operating Systems](On-Board-Computers#operating-systems.md)
  * [Real-Time OS vs. Soft Real-Time OS vs. Everything Else](On-Board-Computers#real-time-os-vs-soft-real-time-os-vs-everything-else.md)
  * [Other Considerations](On-Board-Computers#other-considerations.md)
* [**ROS**](ROS.md)
  * [What is ROS?](ROS.md#what-is-ros)
  * [Why use ROS?](ROS.md#why-use-ros)
  * [Useful Resources](ROS.md#useful-resources)
  * [ROS Commands & Tools](ROS.md#ros-commands--tools)
  * [Writing ROS Nodes](ROS.md#writing-ros-nodes)
  * [Distributed ROS](ROS.md#distributed-ros)
* [**Sensors & Estimation**](Sensors-&-Estimation.md)
  * [Common Sensors](Sensors-&-Estimation#common-sensors.md)
  * [Estimation Techniques](Sensors-&-Estimation#estimation-techniques.md)
* [**Flight Controllers & Autopilots**](Flight-Controllers-&-Autopilots.md)
  * [What is a Flight Controller?](Flight-Controllers-&-Autopilots#what-is-a-flight-controller.md)
  * [What is an Autopilot?](Flight-Controllers-&-Autopilots#what-is-an-autopilot.md)
  * [Control Techniques](Flight-Controllers-&-Autopilots#control-techniques.md)
  * [Comparison of Common Flight Controllers & Autopilots](Flight-Controllers-&-Autopilots#comparison-of-common-flight-controllers--autopilots.md)
  * [Other Considerations](Flight-Controllers-&-Autopilots#other-considerations.md)
* [**Control in ROS**](Control-in-ROS.md)
  * [Control Theory](Control-in-ROS#control-theory.md)
  * [Comparison of Control Methods](Control-in-ROS#comparison-of-control-methods.md)
  * [Analysis of a PID Controller](Control-in-ROS#analysis-of-a-pid-controller.md)
  * [Simple Controller Example](Control-in-ROS#simple-controller-example.md)
  * [Better Controller Example](Control-in-ROS#better-controller-example.md)
  * [A Note On Reliability](Control-in-ROS#a-note-on-reliability.md)
* [**Navigation & Localization**](Navigation-&-Localization.md)
  * [Navigation Techniques](Navigation-&-Localization#navigation-techniques.md)
  * [Grid-Based Navigation](Navigation-&-Localization#grid-based-navigation.md)
  * [Localization Methods](Navigation-&-Localization#localization-methods.md)
* [**Communications**](Communications.md)
  * [Protocols](Communications#protocols.md)
  * [Hardware](Communications#hardware.md)
  * [Sending Information](Communications#sending-information.md)
* [**Airframes**](Airframes.md)
  * [Reference Documents](Airframes#reference-documents.md)
  * [Fixed-Wing](Airframes#fixed-wing.md)
  * [Multirotor](Airframes#multirotor.md)
* [**Power Systems**](Power-Systems.md)
  * [Redundancy vs. Simplicity](Power-Systems#redundancy-vs-simplicity.md)
  * [Comparison of Batteries](Power-Systems#comparison-of-batteries.md)
* [**Image Processing**](Image-Processing.md)
  * [What is OpenCV?](Image-Processing#what-is-opencv.md)
  * [OpenCV & ROS](Image-Processing#opencv--ros.md)
  * [On-Board vs. Off-Board](Image-Processing#on-board-vs-off-board.md)
  * [Development Suggestions](Image-Processing#development-suggestions.md)
  * [Code Snippets](Image-Processing#code-snippets.md)
  * [Optimizations](Image-Processing#optimizations.md)
* [**Web Interfaces**](Web-Interfaces.md)
  * [Setting up the LAMP Stack](Web-Interfaces#setting-up-the-lamp-stack.md)
  * [Configuring MySQL](Web-Interfaces#configuring-mysql.md)
* [**Simulation**](Simulation.md)
  * [UAVTAQ Emulator](Simulation#uavtaq-emulator.md)
  * [Robin Simulations](Simulation#robin-simulations.md)
  * [PX4 Simulations](Simulation#px4-simulations.md)
* [**UAV Setup Guides**](UAV-Setup-Guides.md)
  * [Basic configuration](UAV-Setup-Guides#basic-configuration.md)
  * [MAVROS First Steps](UAV-Setup-Guides#mavros-first-steps.md)
  * [Making Sure Indoor PX4-Based UAVs do not Fly into the Roof if Something Goes Wrong](UAV-Setup-Guides#making-sure-indoor-px4-based-uavs-do-not-fly-into-the-roof-if-something-goes-wrong.md)
  * [UAV Attitude Control through Teleoperation](UAV-Setup-Guides#uav-attitude-control-through-teleoperation.md)
  * [UAV Position Control with the QUTAS Flight Stack](UAV-Setup-Guides#uav-position-control-with-the-qutas-flight-stack.md)
  * [UAV Position Control with the PX4 Firmware](https://github.com/qutas/info/wiki/UAV-Setup-Guides#uav-position-control-with-the-px4-firmware.md)
-->
{% endblock %}
