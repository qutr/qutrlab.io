{% extends "/CPAGE.md" %}

{% block pageContent %}

## Basic configuration
As of time of writing, a basic configuration for an UAV with an on-board computer would use the following pieces of software to operate:
* Operating System (Linux) - ideally some version of Ubuntu for best support with ROS
* Robotic Operating System (ROS) - to allow modular and expandable software development
* An flight controller / autopilot bridge - to allow communication with some form of a low-level controller
* Navigation software - to command the flight controller on what to do
* Data collection / processing - whatever else is required for the UAV to complete it's task

With this "typical" type of setup, there can be many choices for hardware, but it usually looks something like the following:
* Airframe - any type of autopilot-supported configuration
* Autopilot - PixHawk (or MavLink compatible)
* Computer - a SoC-type device (capable of running Ubuntu)

## MAVROS First Steps
It is assumed that you have configured your autopilot, installed ROS, and set up a catkin workspace (located at ~/catkin_ws).

Interfacing the autopilot with ROS is the next big step. To do this, the MAVROS software is highly recommended.
#### Installation
On Ubuntu, MAVROS is supported and available through the official ROS repositories. There are 2 main packages, "mavros" and "mavros-extras". It is recommended that both are installed.

For example, in ROS Kinetic:
```sh
sudo apt install ros-kinetic-mavros ros-kinetic-mavros-extras
```

From here, it is recommended that you look at the [MAVROS ROS Wiki](http://wiki.ros.org/mavros) to learn about each of the different plugins that are available.

#### Configuration
First up, you must [enable the companion computer interface](https://dev.px4.io/en/ros/offboard_control.html) on your autopilot. Now, plug autopilot's Telem2 Port into the computer using a USB-Serial adapter, and find out the device name (it should look something like "/dev/ttyUSB0").

MAVROS comes with a selection of launch files that you can use to connect straight to the UAV. It is recommended you copy these to a local directory so you can edit them freely without having to worry about your setup getting erased when you perform an update. The following commands will create an area to keep your personal launch files, and copy in the template MAVROS ones.
```sh
mkdir -p ~/catkin_ws/launch
cd ~/catkin_ws/launch

roscp mavros px4.launch ./
roscp mavros px4_pluginlists.yaml ./
roscp mavros px4_config.yaml ./
```
The three files that have been copied into your launch directory do the following:
* px4.launch: is the generic launch file
* px4_pluginlists.yaml: a whitelist or blacklist of plugins for MAVROS to load ([check here](https://gist.github.com/pryre/109f13a93b9ea2ba9f17b20ad6b259ef) for a good starting point)
* px4_config.yaml: a configuration file for every plugin

You will need to change a few things before you can actually launch MAVROS. First off, you have to tell the launch file where to look to find the `pluginlists` and `config` files. Open the launch file, and look for the lines:
```xml
<arg name="pluginlists_yaml" value="$(find mavros)/launch/px4_pluginlists.yaml" />
<arg name="config_yaml" value="$(find mavros)/launch/px4_config.yaml" />
```
These must be changed to the directory where your local copies are. For example:
```xml
<arg name="pluginlists_yaml" value="/home/USERNAME/catkin_ws/launch/px4_pluginlists.yaml" />
<arg name="config_yaml" value="/home/USERNAME/catkin_ws/launch/px4_config.yaml" />
```
Secondly, you will need to tell MAVROS what port and baud rate to use. Look for the line:
```xml
<arg name="fcu_url" default="/dev/ttyACM0:57600" />
```
And change it to match your setup. For example:
```xml
<arg name="fcu_url" default="/dev/ttyUSB0:921600" />
```
With the launch files configured, you can run MAVROS with the following command:
```sh
roslaunch ~/catkin_ws/launch/px4.launch
```
As MAVROS starts, it will spit out a lot of messages. The last message that will be displayed before the autopilot is connected will say something along the lines of:
```
MAVROS started. MY ID 1.240, TARGET ID 1.1
```
If you get more messages after that, then your configuration worked! If not, the you may have got your serial port or baud rate incorrect.


## Making Sure Indoor PX4-Based UAVs do not Fly into the Roof if Something Goes Wrong
By default, the PX4 Firmware failsafes are set to "Return to Launch" in the case of most emergencies. With the default parameters, this also means that the UAV will attempt to fly up to 30m, and navigate back to [0,0,30] before attempting a landing procedure.

The ideal is to have the failsafe in any condition to land immediately.
[Failsafe Settings](https://docs.qgroundcontrol.com/en/SetupView/Safety.html)

If you are performing any type of navigation, you should be able to use the custom mode "AUTO.LAND" to request an immediate landing.

---
**Note:** Due to the constantly changing firmware, you should ensure you have some sort of plan in case of an emergency (i.e. manual override). There are many different parameters that control "AUTO" modes, so ensure you have set any that may relate to the modes you are trying to work with.

---

## UAV Attitude Control through Teleoperation
Before we continue, it is assumed that you are using a flight controller running a firmware that is compatible with the SET_ATTITUDE_TARGET MAVLINK message (such as the [PX4 Firmware](http://github.com/px4/Firmware) or [robin](http://github.com/qutas/robin)). It is also assumed that you have MAVROS up and running on, with a stable connection to the flight controller, you have enabled the setpoint_raw MAVROS plugin, and that you have a catkin workspace configured and ready to use.

Firstly, download and compile the _teleop\_offboard\_attitude_ package:
```sh
cd ~/catkin_ws/src
git clone https://github.com/qutas/teleop_offboard_attitude
cd ~/catkin_ws
catkin_make
source ~/catkin_ws/devel/setup.bash
```

Before we attempt a flight, it is a good idea to ensure that the joystick is operating in the right frame of reference, and is rotating as expected.

Copy the launch file to a local directory, and configure it:
```sh
cd ~/catkin_ws/launch
roscp teleop_offboard_attitude teleop.launch ./
nano teleop.launch
```

For most flight controllers with a compass, the frame of reference for controlling the UAV must be the _world_ frame, so change the _frame\_id_ parameter to match. You can use the _axis/roll_, etc., parameters to set the joystick axis to match the expected function of the joystick. By default, this launch file _should_ be almost perfect for most low-cost joysticks (such as the "Logitech Extreme 3D Pro"). You should also configure _joy_ package parameters to select the correct joystick as well.

---
**Note:** If using a px4-based firmware, as of time of writing, they currently do not support mixed control of angular and rate setpoints. Be aware that yaw control may not work. i.e even if your joystick supports yaw, yaw rate commands will not work with angular commands for roll and pitch mode. Expect the UAV to orient with North on takeoff.

---
To test the configuration of the joystick, run the node, open rviz, and view the output pose of the teleop node:
In terminal 1:
```sh
roslaunch ~/catkin_ws/launch/teleop.launch
```
In terminal 2:
```sh
rviz
```
Add in a pose plugin, and set the topic to (by default `/teleop/goal/attitude`). Moving the joystick should show a representation of the joystick responding accordingly. This will not display rate commands. It is recommended that you check the attitude setpoint topic (by default `/mavros/setpoint_raw/attitude`) to ensure that moving other axes of the joystick (e.g. throttle, yaw) respond accordingly. Remember that all inputs in this mode are in the ENU frame.

**Note:** The _teleop\_offboard\_attitude_ node will not output any commands until the joystick is first moved. Additionally, it will set all axis to their center point (i.e. thrust will be set to 0.5 when you first bump the remote). This is due to the way joysticks interface in Linux and is unavoidable. **For this reason, it is highly recommended that when you first start the node, you move the throttle axis, and reset it to 0 before continuing.**

Next, prepare the UAV for flight and launch MAVROS. As your environment will most likely quite unique, this part won't have a guide.

Once you have your environment up and running, launch the telop node (if it isn't running already), disarm the safety on the UAV, and run the following commands to arm the UAV:

---
**Note:** the flight controller may safety mechanisms inbuilt to ensure that a UAV is not left armed. Usually a flight controller will expect some kind of throttle input within 5 seconds of arming, otherwise it will disarm itself automatically. It is recommended that if you need a little time before take-off, but after arming the UAV, you should bump the throttle slightly then set it to 0 to ensure the UAV does not automatically disarm.

---
**PX4 Only**

With the PX4 firmware, we first must request off-board mode before continuing:
```
rosrun mavros mavsys mode -c OFFBOARD
```

**Robin Only**

The default control mode for _robin_ is off-board mode. Ensure that the attitude setpoints have been accepted by the flight controller before continuing. The following message (or something similar should be in the MAVROS logs):
```
[FCU] Off-board control connected!
```

**All**

It is recommended that you prepare another terminal with the disarm command ready, in case of emergency:
```sh
rosrun mavros mavsafety disarm
```

Arm the UAV, then you should be in full control:
```sh
rosrun mavros mavsafety arm
```

Remember to disarm the UAV after the flight is completed.

## UAV Position Control with the QUTAS Flight Stack
Due to the intricacies of using GPS enabled autopilots indoors, better results may be achieved using a ROS-based control stack to perform indoor position control of an autopilot. This provides the additional benefit being able to use a cheap flight controller rather than a complete autopilot.

There are a few major downsides however:
- Depending on your ideal autopilot choice, it may be some work to switch from the QUTAS Flight Stack
- Due to the need to run flight-critical software off-board the flight controller, the on-board computer must be powerful, or a low-latency, highly reliable communication link must exist to the ground station

![Example_Flight_Stack](qutas_flight_stack.png)

As an example that may represent a typical indoor flying area, you will at least need to following:
- A position measurement (or a bridge from a motion capture system. e.g. vicon\_bridge)
- An odometry estimation (using _pose\_odom\_node from the _mavel_ package)
- Some form of navigation software (a simple waypoint script for example)
- A position controller (using _mavel_)
- An autopilot bridge (_mavel_ was developed with _mavros_ in mind, but anything that accepts an attitude command could work)

![Mavel flowchart](mavel_flowchart.png)

To use the _mavel_ package, you must give it a stable steam of odometry messages, as well as some form of navigation goals. The navigation goals, or setpoints, can be either position, velocity, or acceleration. The control is performed in the frame the odometry message is given in (e.g. world frame), so ensure that the setpoints are also in this frame.

Once a stream of odometry measurements and setpoint commands has been established, then mavel will begin transmission of attitude and thrust setpoints. It is recommended that the navigation either does not send navigation goals until the UAV is armed and ready to fly (_mavel_ will send 0 attitude and thrust commands from launch to establish a steam with the autopilot).

#### Configuring an Autopilot
Depending on the autopilot you decide to use, you may need to configure it to be on the same frame of reference as what mavel is operation on (i.e. the same frame as the motion capture system, usually "map").

In reality, this usually means sending information from your motion capture system to the autopilot so it can extract an heading reading (such that it knows which way is forward). If you are using _mavros_, you should be able to send the pose as measured by the motion capture system to the [_mocap\_pose\_estimate_ plugin](http://wiki.ros.org/mavros_extras#mocap_pose_estimate), and it will handle the rest. There may also be some configuring of the autopilot to ensure it accepts this estimate, so make sure you refer to the autopilot documents for more information on this.

## UAV Position Control with the PX4 Firmware
TODO

#### Temporary Resources
* [External Position Estimation](https://dev.px4.io/en/ros/external_position_estimation.html)
* [MAVROS Mocap Pose Plugin](http://wiki.ros.org/mavros_extras#mocap_pose_estimate)
* [Offboard Control](https://dev.px4.io/en/ros/offboard_control.html)
* [MAVROS Offboard Example](https://dev.px4.io/en/ros/mavros_offboard.html)
* [QUTAS Sample Code for MAVROS Offboard](https://github.com/qutas/kinetic_sample_packages/tree/master/kinetic_sample_mavros_guider)

{% endblock %}
