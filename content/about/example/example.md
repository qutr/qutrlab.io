{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Example Content Page
This page shows an example of a various content layouts that can be used on the QUTR Wiki. Before starting, make sure you are familiar with the [Markdown layout](http://commonmark.org/help/)

## Headings
First off, you'll notice that this page starts with a `Heading 1` style tag. At the start of each new page, a `Heading 1` should be used, and should match the link used in the `SUMMARY.md` page. If you find that you would like to use another `Heading 1` tag, you should consider creating another page. Similarly, you will notice that we use the `Heading 2` to `Heading 4` tags for subsections, an so on. If you find you need more than 4 headings, you should look into creating another page.

## Linking Pages
Links can be created in the Wiki using the linking format `[Link](http://example.com)`. If you would like to link to a page within the Wiki itself, you can use either relative links, `[Example](example.md)`, or absolute links, `[Example](/about/example/example.md)`, to any other content.

Likewise, you can also link to specific headings within a page using the format `[Linking Pages](example.md#linking-pages)`, [for example](example.md#linking-pages).

## Inline & Block Code
To display code, you can use backticks `` ` ``. Putting text within backticks will make it be displayed `just like this`. In addition, we can use a set of 3 backticks in a row to define a section of block code. Block code can also be syntax highlighted to code; to write some code in C++:
````
```c++
int i = 0;
i = 5 * 10;
i++;
```
````
```c++
int i = 0;
i = 5 * 10;
i++;
```
You should use syntax highlighting wherever possible in your content.

## Including Images

![QUT Logo](https://www.qut.edu.au/qut-logo-og-200.jpg)

To get more specific image formatting (like controlling the rendering size) we can embed HTML:
```HTML
<center>
<img src="https://www.qut.edu.au/qut-logo-og-200.jpg" alt="QUT" height="50">
</center>
```
<center>
<img src="https://www.qut.edu.au/qut-logo-og-200.jpg" alt="QUT" height="50">
</center>

## Advanced GitBook content
The GitBooks toolchain allows for a lot of possibilites regarding how content is layed out. For more information, please check their own [GitBook](https://toolchain.gitbook.com/)!

## GitBook Plugins
Aside from the base GitBook extensions, you will also have access to a number of additional plugins.

### Alerts
#### Info styling
```Markdown
> **[info] Info**
>
> Use this for infomation messages.
```
> **[info] Info**
>
> Use this for infomation messages.

#### Warning styling
```Markdown
> **[warning] Warning**
>
> Use this for warning messages.
```
> **[warning] Warning**
>
> Use this for warning messages.

#### Danger styling
```Markdown
> **[danger] Danger**
>
> Use this for danger messages.
```
> **[danger] Danger**
>
> Use this for danger messages.

#### Success styling
```Markdown
> **[success] Success**
>
> Use this for success messages.
```
> **[success] Success**
>
> Use this for success messages.

{% endblock %}
