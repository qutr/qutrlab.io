{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Requesting Content
As the information in this document is a shared effort to keep up-to-date and relevant, some things may need to be updated at some point. If you feel there is an issue with any of the pages in the repository, whether the content is out-of-date, incorrect, or just needs some extra clarification, it is appreciated if you could create an issue on [the repository](https://gitlab.com/qutr/qutr.gitlab.io/).

To creat an issue, simply log in with your GitLab account, navigate to the [the repository](https://gitlab.com/qutr/qutr.gitlab.io/), and press the `+` icon then click "Create new issue". From here, select an issue template as appropriate in the title section, then fill out the issue.

Please leave the "Assignee", "Milestone", "Labels", "Weight", and "Due date" sections blank, these will be filled in later by the administrator.

Now go ahead and press "Submit Issue".

{% endblock %}
