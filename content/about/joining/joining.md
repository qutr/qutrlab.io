{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Joining QUTR

## Requesting access to the GitLab Group
1. Create an account on [GitLab](https://gitlab.com/)
2. Go to the [QUT Robotics](https://gitlab.com/qutr) page and request access

## Uploading your code
There are multiple ways to put your code onto GitLab under a personal project.

> **[warning] Warning**
>
> During the next few steps, it is possible to create a repository as private, such that only members or groups that you allow will have access to it.
>
> Please be resposible when uploading your code, and be aware of any risks if you are handling sensitive material.

### Creating a new repository
[Create a new project](https://docs.gitlab.com/ce/gitlab-basics/create-project.html) through GitLab.

### Migrating
[GitLab Migrtation Guide](https://docs.gitlab.com/ce/user/project/import/index.html)

## Share your project with the QUTR group
Before you can share a project with QUTR you have to add it as a personal repository on GitLab, so make sure you do the previous steps first!

Once you have created a repository on GitLab, please follow [these instructions](https://docs.gitlab.com/ce/user/project/members/share_project_with_groups.html#share-projects-with-other-groups) on how to share access to your repository with a group. It is recommended that you set the **maximum access level** to **Reporter**, as this will allow a decent level of interaction with other users without having to worry about compromising any of your code. If you would like to colaborate with a user on a repository you can request developer access to that repository directly, or you would like to give a user a higher access level you can follow [this guide](https://docs.gitlab.com/ce/user/project/members/).


> **[warning] Warning**
>
> If you created a repository as private, then, in theory, it should only be visible to the users and groups you add. Remember to be cautious if your code is sensitve.

{% endblock %}
