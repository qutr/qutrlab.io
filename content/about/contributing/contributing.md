{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre", "@example-maintainer"] %}

{% block pageContent %}

# Contributing to the Wiki

## Making a contribution

## Getting your content added
1. Go to [the repository](https://gitlab.com/qutr/qutr.gitlab.io) and fork it to your own account
2. Create a new branch with `git checkout -b private-branch`
3. Run `git pull` to get the latest version of the repository
4. Make some changes (preferably to one page at a time)
5. Commit your changes
6. Go to [the repository](https://gitlab.com/qutr/qutr.gitlab.io) and create a new merge request
7. If you would like to make further changes, repeat from step 3.

## File structure
Within the Wiki respository, each content page follows the following rules:
1. Each section should be within its own folder
2. All content added for a page should go in the folder for the page it was first added for.
3. Each subsection should be in a subfolder within the parent folder
4. All sections should be added to the `SUMMARY.md` page so that they can be tracked

## Using the CPAGE template
In the Wiki, we make use of templating to keep a consistant wrapper around the content. This is achieved with the `extends` macro in GitBook. To use this when building your own page, simply wrap your code like so:

{% raw %}
```
{% extends "/CPAGE.md" %}

{% block pageContent %}

# Your content here!

{% endblock %}
```
{% endraw %}

## Signing your content

> **[warning] Warning**
>
> Due to the way git works, please remember that all content that you upload will be able to be traced back to your account.

As mentioned in [Requesting Content](/about/request/request.md), people may like to get additional clarification, or to request that page is updated. To allow the administrators to be in contact with you regarding the content that you have created, it is appreciated if you would opt-in to putting your GitLab username (check the top of this pages source for an example) at the bottom of pages you are willing to support and/or update.

You should not be contacted directly through this by other users directly. If this happens, please let us know in person.

{% endblock %}
