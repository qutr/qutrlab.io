{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# About QUT Robotics

## Points of Contact
If you have any issues that should be dealt with immidiately, please contact one of the following people in person or fia email:
- Jonathan Roberts
- Kye Morton

## Contributers
- Kye Morton ([@pryre](https://www.gitlab.com/pryre))

{% endblock %}
