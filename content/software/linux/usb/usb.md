{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# USB Devices
In Linux, there is some devices that may not be accessible without the correct permissions.

### USB Flash Drives
Unlike a normal desktop computer set up to be used in a GUI, Linux won't automatically mount a USB flash drive so it can be accessed by a user.

First we must create a mount point (a path that will allow us to access the device). Then we have to give the correct permissions so a user can read and write to the device (in this case, we give read/write for all users).
```sh
sudo mkdir /media/usb
sudo chmod  g+rw,o+rw,u+rw /media/usb
```
Next we can mount the device to the path we created (assuming you know the device and partition you want to mount).
```sh
sudo mount /dev/sdX /media/usb
```
Lastly, remember to un-mount the device before you remove it from the system (we also use sync to check all data has been transferred)
```sh
sync
sudo umount /media/usb
```

### Serial Adapters
Before we can use a serial device such as a USB-Serial Converter, we must add our user to the "dialout" group. This will allow us to use any "/dev/ttyX" device.
```sh
sudo usermod -aG dialout $USER
```
Remember to log out and back in to make sure the group permissions are updated.

If we are using a pre-installed system, it is a good idea to remove the "modem manager" package, as it tends to really mess with serial communications. As an example for Ubuntu:
```sh
sudo apt remove modemmanager
```

### Cameras & Video
Before we can use a video device such as a webcam, we must add our user to the "video" group. This will allow us to use any "/dev/videoX" device.
```sh
sudo usermod -aG video $USER
```
Remember to log out and back in to make sure the group permissions are updated.

{% endblock %}
