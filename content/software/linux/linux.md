{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Linux
## Linux in a Nutshell
If you have any plans to work with robotics, or high-performance systems, you are most likely looking at using Linux in some form. Linux is not much different from other operating systems (such as Windows or OSX), but there are a few key differences in its design. There are two main design ideologies: "The Unix Philosophy", and "Everything is a File". Aside from these points, the front end of Linux system can be made to feel very similar to other operating systems.

## The Unix Philosophy:
- Write programs that do one thing and do it well
- Write programs to work together
- Write programs to handle text streams, because that is a universal interface

## Everything is a File
The preliminary idea of "Everything is a File" is that all items in the system, documents, hard-drives, keyboards, printers, network sockets, etc., are simply streams of data. This approach allows many types of tools to integrate easily with all sorts of devices without any need for specific interfaces. It also allows for many general tools for searching and filtering data that are compatible with nearly anything you could use them for.

## GUI vs. CLI
Within the Linux community, there is a fairly large split between those who prefer the Graphical User Interface (GUI), and those for the Command Line Interface (CLI). Either way, the environment is still Linux. The preference for a normal user will come down to both the device, and the usage of the environment.

For example, it is recommended for on-board computers, which will be running most of the time with out a screen plugged in, to be run without any sort of GUI environment set to startup by default, as it will save computing resources for needed computations. For a full sized computer, such as one that will be used to develop software on, it is probably a good idea to run a GUI to make life easier for the developer.

By this point, you have probably heard of the Ubuntu distribution. Like all other Linux distributions (to be brief), Ubuntu is simply a collection of supported software that is packaged together in a certain manner. Going from here, there are multiple desktop environments that can be used, each with different levels of ability. In Ubuntu, there are multiple versions of prepackaged desktop environments.

| Name     | Ubuntu Distro  | Description                                            |
| -------- | -------------- | ------------------------------------------------------ |
| GNOME    | [Ubuntu](https://ubuntu.com/download/desktop)  | A feature packed desktop for maximum user friendliness |
| KDE      | [Kubuntu](https://kubuntu.org/getkubuntu/)     | A sleek desktop with a mix of usability and speed      |
| **XFCE** | [**Xubuntu**](https://xubuntu.org/getxubuntu/) | **A light-weight full-featured desktop**               |
| LXDE     | [Lubuntu](http://lubuntu.net/)                 | The most light-weight full-featured desktop            |

For the first entrance into Linux, the desktop environment **XFCE** is recommended for a few reasons: it is very familiar for most users, it runs well on most devices, and it is highly configurable. For SoC type devices, if a GUI is desired, LXDE is recommended as it is the least processor-intensive out of all the options.

Do not be afraid to pick one without too much background knowledge, as it is possible to install and test multiple at once, to get familiar with the options without any pressure.

Regardless of the choice to use a GUI or CLI, you will no doubt use a a terminal at some point throughout your experience with Linux, and as such, you should ensure you're comfortable with the command line. If you are using a GUI, you should try to get the hang of navigating around and doing basic tasks, such as doing some basic file editing or installing software, in a Virtual Terminal so that you are able to at least familiar enough to set up a GUI from the CLI or connect to a remote device over SSH.

Some basic CLI tools to remember (you can use the "-h" and "--help" options to find out more information):
* **ls** - List directory
* **cd** - Change directory
* **cp** - Copy file
* **mv** - Move (or rename) file
* **cat** - Concatenates (prints) a files contents
* **nano** - A basic text editor (use "CTRL+X" to exit)

Some file system notes:
* You can reference the current directory with `.` (e.g. `nano ./notes.txt`)
* You can reference the parent directory with `..` (e.g. `nano ../parent_notes.txt`)
* You can reference the home directory with `~` (e.g. `nano ~/my_notes.txt`)
* You can stack directory references (e.g. `cd ../../` to go up two directory levels)
* Hidden files and directories start with a `.` (e.g. `nano ~/.bashrc` to access a hidden file in the home directory)

Some more advanced tools:
* **tmux** - Allows for detached sessions (keeps processes alive if disconnected), terminals in 1 window, multiple windows, etc.

## Preparing a Work Environment
Before we can dive into using Linux, we will need to install it in some manner.

### The "Non-Committal" Way
The easiest way to try Linux in a semi-permanent fashion is to use virtualization. The typical method would be to use a hypervisor, such as "VirtualBox", to effectively emulate the entire system. This method will only create a Virtual Machine which exist as files on your hard drive, and are therefore very easy to uninstall when you are done with

As a suggestion, go check out [VirtualBox](https://www.virtualbox.org/) for further information.

There are main considerations that you should check before using a Virtual Machine:
* A VM will make it difficult (but not impossible) to communicate from a remote computer to the guest computer.
* VM support for 3D software (specifically software using OpenGL or DirectX) will most likely not run inside the VM (although there is some exceptions).
* They provide a super-easy backup solution (after you install ROS, etc., you can copy-paste the VM to make a backup. You can also make Snapshots of the VM to roll back to at a later date).
* They provide a risk-free environment (nothing done on the Guest should be able to affect Host, also provides immediate protection from accidentally deleting files on the Host)
* Guest machines should not be used for anything that is time-critical (real time computations can be delayed significantly if the Host is under high load, and the Guest may think it is still running in real-time)

One critical note to allow proper networking to the Virtual Machine, you will have to change the networking mode on to bridge networking (the exact setting in VirtualBox is labeled as "Bridged Adapter").

### The "Right" Way
The "proper" way to install Linux is by actually installing it to the hard drive. This will get around all the disadvantages that the Virtual Machines suffer, but does require a bit more commitment and setup on the software side. If you are all ready running an operating system, such as Windows, your best option is to look into "dual booting", which will allow you to switch back and forth between the two systems.

For further information on a good place to start, go check out the [Ubuntu Dual Boot guide](https://help.ubuntu.com/community/WindowsDualBoot) for further information.

{% endblock %}

