{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Proccess Priority
When you run a program, it will generally be set to a priority common to all other software that has been run at a user level (i.e. not system-critical software). This means that all user-run processes will be given an equal chance to perform any necessary calculations. It may however be desirable for a program that is will use up a lot of resources, if given the chance, to run at a lower priority than other programs, to allow for a fairer sharing of resources. Likewise, it may be desirable for some mission-critical programs to run at a higher priority, to ensure that they are operating with the best possible conditions.

In Linux, the priority of a process is controlled through two different scales. For most use cases, the scale of _niceness_ is what we can use to tell Linux to pay more attention to a process, or to be more likely to give it system resources when requested. The _niceness_ is a scale of -20 to +19, where; -20 will tell Linux to treat it with the highest priority, 0 is the default, and 19 will tell Linux that the process can but put on hold if a more important process needs to run.

To run a program with a lower priority (a python script for example):
```sh
nice -10 python script.py
```
Or with a higher priority:
```sh
nice --10 python script.py
```

To reassign a program a different priority:
```sh
renice -n 10 -p 3534
```
Where `-n 10` will set the niceness value to `10`, and `-p 3534` is the target process ID.

It may be possible to use pgrep to get the process name (take care using this in practice):
```sh
renice -n -10 -p $(pgrep some_program_name)
```

To set a negative niceness, you will have to execute the command as a root user (with `sudo`).

The weight is roughly equivalent to 1024 / (1.25 ^ nice_value). As the nice value decreases the weight increases exponentially. The time-slice allocated for the process is proportional to the weight of the process divided by the total weight of all run-able processes ([Reference](https://unix.stackexchange.com/questions/89909/how-is-nice-working/90344#90344)).

> **[warning] Warning**
>
> You should take care of assigning the niceness value. Setting a niceness of -20 may cause your programs to interfere with system-level programs (not good!). It is recommended that you set mission-critical programs to something around -10 to -15, and set non-critcal programs to something around +10 to +15. This way, leaving other programs that run at the default user level (0) will still remain in good shape, and you won't interfere with system-critical programs.


{% endblock %}
