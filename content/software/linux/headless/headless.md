{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Remote Access
If you need to access or run a computer remotely, or withough using monitor, this is usually referred to as using the computer in headless mode.

## Serial Access
As a start point, or as a reliable backup, it may be desirable to configure a login prompt over a hardware serial line. In modern systems, first you must identify the serial port (in this example, we want to start the serial on "/dev/ttyS2"), then it should be as simple as running the following commands:
```sh
systemctl enable serial-getty@ttyS2.service
systemctl start serial-getty@ttyS2.service
```
Or (as an example, to use the header Tx/Rx on a Raspberry Pi):
```sh
systemctl enable serial-getty@ttyACM0.service
systemctl start serial-getty@ttyACM0.service
```

## SSH
For most systems that are running without some form of easy access (e.g. an on-board computer), it is often desirable to have some form of method to access them remotely. The most versatile method is by using Secure Shell (SSH). SSH is a tool that allows us to log in as any user through a remote IP address.

With most distributed OS images, SSH will be enabled by default, all that is needed to log in is a username/password and the remote system's IP address. Most Linux systems will come with the SSH client pre-installed, and if the local system is running something like Windows, a program like "Putty" will need to be used. On Linux, simply run the command:
```sh
ssh user@remote-ip
```
For more powerful systems, it is also possible to achieve a Virtual Remote Connection (VNC), which will allow access to a desktop GUI on the remote system. As a good starting point, look into "TigerVNC" for more information.


{% endblock %}
