{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Networking
Networking in Linux is a fairly open experience, as the system will allow nearly any combination of configurations if it is set up correctly. Unfortunately, such a setup takes time, and may not be the most appropriate for a system that can afford to be much simpler.

The best way to list the available networking devices is with the following command:
```
ifconfig
```

## Quick Setup
For most devices, the best place to start is by getting the ethernet port configured (if it isn't already). For most Ubuntu-type systems, there are two primary options for getting a basic connection up and running.

Going forward, if you are on a fresh system, you may find that there one device as already in the following files, the "loopback" interface. It will look something like this for reference:
```
auto lo
iface lo inet loopback
```

> **[danger] Danger**
>
> The loopback device should not be disabled under any circumstance, as your terminal and many other system functions rely on this exclusively.
>
> If it is somehow disabled, you will most likely have to edit the file using another comupter.

For most backwards compatibility, edit the file "/etc/interfaces" and ensure the following lines are there.

For DHCP (best when using some kind of router):
```
auto eth0
iface eth0 inet dhcp
```
For a static address (best if you need to do a direct connection):
```
iface eth0 inet static
address 192.168.1.2
netmask 255.255.255.0
gateway 192.168.1.254
```
The system will need to be rebooted for the settings to be applied.

For newer systems, the folder "/etc/interfaces.d/" can be used to configure multiple setups and devices. To use this option, simply create a file "/etc/interfaces.d/eth0.cfg" and add the same lines as above to it. This won't be covered here for brevity.

## Automated/Managed
Before continuing on with managed network solutions, there is a very important point to make: **ONLY ONE SOLUTION CAN BE USED AT A TIME**. If multiple solutions are used, there **WILL** be issues with your network connectivity. This means that once you install a network manager, you must disable the lines for devices configured in the "Quick Setup" process.

Before continuing, note that a user may need to be in the "netdev" group to control these network managers:
```sh
sudo usermod -aG netdev $USER
```
Remember to log out and back in to make sure the group permissions are updated.

As far as network managers go, there are 3 main contenders (as of time of writing):
* **NetworkManager** - A very configurable, expandable, and scalable network manager
  * Start with `sudo systemctl start NetworkManager.service`
  * Can be controlled through the command line with `nmcli`
  * Can be controlled through a GUI with `nm-applet`
  * Can be configured to run multiple connections at once
* **wicd** - A simple, reliable, and scripting-friendly network manager
  * Start with `sudo systemctl start wicd.service`
  * Can be controlled through the command line with `wicd-cli`
  * Can be controlled through the command line as an interface with `wicd-curses`
  * Can be controlled through a GUI with `wicd-gtk`
  * Can only have one connection active at any one time
* **connman** - A light-weight networking solution aimed at ARM-based devices
  * Start with `sudo systemctl start connman.service`
  * Can be controlled through the command line with `connmanctl`
  * Can be controlled through a GUI with `cmst`
  * Can only have one connection active at any one time

**Out of personal experience, wicd provides the most reliable and easiest to configure network manager``

## Wireless Networking
Wireless networking can be configured in multiple different ways, depending on the application of your system.

### Using a Router
The easiest way to configure and use one or more wireless devices is to have some form of wireless-capable router. This will allow you to to many different configurations, and will usually handle DHCP, so you do not need to worry about static IPs

### Ad Hoc
Allows the creation a dynamic networks using only the wireless chips on the devices that are connecting to the network. This is very useful when trying to create a network where multiple devices need to talk to each other in the field, or where using a router will be inconvenient.

### Hotspots
A newer configuration where a single device will act as a host/router, allowing other devices to connect to it and will assign IP addresses to each device. This is most useful for devices that need to be connected to for initial configuration, but do not need a permanent connection, or as an initial interface before a permanent configuration is set up.

### WiFi issues
Due to the way the 802.11 (WiFi) standard is defined, [there is a random and variable delay for a device to wait while the channel is busy](https://en.wikipedia.org/wiki/Timing_Synchronization_Function). Combined with general noise and interference, this can cause WiFi to be very unreliable, in respect to trying to maintain a low-latency transmission. Out of previous experience, WiFi cannot realistically be relied on for anything that requires a latency <50ms.

Other issues to consider is how to handle disconnections, or devices not connecting in the first place.

#### Power Save Polling
<!--
TODO:
Need to move some of this info to the windows/mac sections
-->

One very common issue with wireless networking is power saving settings on WiFi devices. Most commonly, this is in the form of a setting called Power Save Polling (PSP or PS-Poll). **It is highly recommended that you disable this feature for any type of latency-critical connections).**

The most common symptom of PSP causing latency issues is one-way latency to wireless devices. This can be tested for on a network with a wireless device, and some access to a wired device that supports some form of _ping_ command. If the pings from the wireless device to the wired device look something like the following:
```
Pinging 192.168.0.1 with 32 bytes of data:
Reply from 192.168.0.1: time=3ms
Reply from 192.168.0.1: time=3ms
Reply from 192.168.0.1: time=4ms
Reply from 192.168.0.1: time=6ms
Reply from 192.168.0.1: time=4ms
```
And the pings from the wired device to the wireless device look something like the following:
```
Pinging 192.168.0.2 with 32 bytes of data:
Reply from 192.168.0.2: time=244ms
Reply from 192.168.0.2: time=59ms
Reply from 192.168.0.2: time=79ms
Reply from 192.168.0.2: time=4ms
Reply from 192.168.0.2: time=100ms
```
Then your devices are most likely are suffering from PSP.

For Windows devices:
```
Control Panel > Power Options > Change Plan Settings (for current/all plans)
Wireless Adapter Settings > Power Saving Mode > Setting
Set this to "Maximum Performance"
```

For Mac devices:
```
No fix currently known
```

For Linux devices:
```sh
sudo iw dev wlan0 set power_save off
```
Or:
```sh
sudo iwconfig wlan0 power off
```

{% endblock %}
