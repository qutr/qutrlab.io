{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Time Syncronization
Working on a distributed network can bring some unforeseen consequences to how you have to plan out data processing and handling. The main 2 issues that will affect a network are latency (the time it takes for data to be sent) and time syncing (having 2 independent computers tracking "current time" separately).

To get around this, most complex systems will implement 2 other systems:
* A dedicated local network for the computers to converse over without worrying about other computers clogging up available bandwidth (usually through the use of an router)
* A method for synchronizing system time across multiple devices

To combat the issues of time synchronization, there are many methods that can be used, however, for the sake of simplicity, we will use a client-server model. Fortunately for us, most large computer-based organizations (should) run a time server. Although this is not on the local subnet (or router), we (hopefully) have a gigabit connection to the timeserver through the network, so for the purposes of the occasional sync (as we don't really have to worry about long-term bandwidth congestion), this will do quite nicely for our purposes.

First off, we need to install and start the package called "chrony" on all computers that will be using network. Next, we just have to tell chrony where to look to find the best timeserver. Open the file "/etc/chrony/chrony.conf" (on Ubuntu-type systems) and scroll down to the first line that is uncommented, it should look something like:
```
server 0.pool.ubuntu...
```
OR:
```
pool 0.pool.ubuntu...
```
In either case, comment this line out (add a '#' to the front of the line), and just above it we will enter the location of our time server (QUT's timeserver for example):
```
server time.qut.edu.au iburst
```
This will configure chrony to only look for this server to sync with. If your network blocks external time servers (as QUT does), this means that your PC will only be able to sync with this time server when you're on the network. If you would like to time sync with a public server when you are not on the network (which is probably a good idea anyway), you could uncomment the original time server, but have it second in the list.

Another fairly critical setting to configure is the "makestep" setting. This will tell chrony that it is allowed to perform a large initial jump in time during the first few time syncs. The following setting will large jumps to occur if the time delta is larger than 1.0 seconds, but only on the first 3 time syncs. Find the and change the setting to the following (or just enter it near the top of the config file):
```
makestep 1.0 3
```
Finally, run the following command to restart chrony:
```sh
sudo systemctl restart chrony.service
```
You can now run the following to see what server chrony has synced with (although it may take a few seconds):
```sh
chronyc tracking -a
```
If the server you entered is listed near the top, then success!

{% endblock %}
