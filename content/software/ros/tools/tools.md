{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# ROS Commands & Tools
This section will briefly outline some of the more standard commands and tools available through ROS, and how they can be used.

Unless specifically stated, most of the following tools can be run from any directory, as they will automatically locate the necessary folders/packages within your environment.

## roscore
The _roscore_ is the launches the ROS master interface, which organizes all running Nodes, and will handle service requests, among other things. It must be run to start the ROS environment (with the only exception being that the _roslaunch_ tool will start a _roscore_ in the background if one is not running already).

For more information, check the [ROS Wiki](http://wiki.ros.org/roscore).

### Usage:
```sh
$ roscore
```

## rosrun
The _rosrun_ command will automatically search for, and allow you to launch programs and Nodes from, specific packages. As long as you have your ROS installation sourced (most likely done automatically with your _.bashrc_ file), as well as any catkin workspaces, etc., _rosrun_ will be able to find packages, and launch programs, scripts, etc., from within them.

Many packages will offer helper tools and scripts that can be run with _rosrun_. It is also a good way to quickly test software during development.

For more information, check the [ROS Wiki](http://wiki.ros.org/rosbash#rosrun).

### Usage:
```sh
$ rosrun package_name program_name
```

## roscd / roscp / rosed
These tools can be used to perform specific functions on packages and files within your environment without the need to directly locate them in your filesystem.

### Usage - roscd:
Used to change directory into a package:
```sh
$ roscd package_name
```

### Usage - rosed:
Directly edit a file from a package (you need to have the $EDITOR bash variable set to your text editor of choice first):
```sh
$ rosed package_name file_name.launch
```

### Usage - roscp:
Copy a file from a package to somewhere else (acts very similar to the standard _cp_ command in Linux):
To copy a file to your current directory:
```sh
$ roscp package_name file_name.launch ./
```
To copy a file somewhere else, and rename it if desired (multiple examples):
```sh
$ roscp package_name file_name.launch /home/user/file_name.launch
$ roscp package_name file_name.launch ~/catkin_ws/launch/new_name.launch
```

For more information, check the [ROS Wiki](http://wiki.ros.org/rosbash).

## roslaunch
The _roslaunch_ package allows you to run a launch script to assist with automation, setting Node parameters, as well as many other features. It is extremely helpful when configuring a project that requires many different Nodes to be run, and will manage all of them through a single terminal.

Most packages will provide example launch files which can be used to test or simply as a base to create your own launch files. It is highly recommended that you copy these files to a local directory before you modify them, as any updates to your system (for that package) may overwrite changes to the files.

For more information, check the [ROS Wiki](http://wiki.ros.org/roslaunch).

### Usage:
There are two different ways to run launch files; from a package, or with a file path.

From a package:
```sh
$ roslaunch package_name name.launch
```

With a file path (multiple examples):
```sh
$ roslaunch /home/user/file.launch
$ roslaunch ~/catkin_ws/launch/file.launch
$ roslaunch /usr/share/ros/launch/file.launch
```

## rostopic / rosservice
These packages give access to a collection of tools to perform checks on topics, transferred data, generating fake data, run services from the command line (great for say, remotely arming a UAV, or commanding a mission to begin), as well as a lot of other diagnostic information.

For more information on _rostopic_, check the [ROS Wiki](http://wiki.ros.org/rostopic).

For more information on _rosservice_, check the [ROS Wiki](http://wiki.ros.org/rosservice).

## rqt / rviz
These packages can allow for assembling a nice GUI, or easy visualization of data. both programs allow you to add in additional plugins to enhance the experience.

For more information on _rqt_, check the [ROS Wiki](http://wiki.ros.org/rqt).

For more information on _rviz_, check the [ROS Wiki](http://wiki.ros.org/rviz).


{% endblock %}
