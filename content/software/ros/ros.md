{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Robotic Operating System

## What is ROS?
The Robotic Operating System (ROS) is a management solution for real-time robotic software. ROS provides simple and robust interfaces for communication between software, while allowing for the complete logging of the system. Most popular software packages, such as OpenCV, Mavlink, Python, and MatLab, have a ROS compatible component to allow for easy integration. Additionally, there is a huge amount of user packages available on the community network.

## Why use ROS?
The primary usefulness of ROS is in the libraries available as part of the ROS environment, and how simple they make it to develop modular and interchangeable software. The idea behind developing software in this way is that it allows us to develop both tools that are useful for multiple use cases, as well as tools that are very good at solving a specific problem with as little complexity in the specific node, which aids in development, debugging, and understanding for others. For the most systems, there is no noticeable delay within the system, and if best conventions are followed, a highly dynamic system can be developed without many losses in performance.

The use of different Nodes to handle different problems also allows ease of splitting and integrating intermediate software or data visualizations without any additional programming. It also allows use to collect and analyze data after the experiment and replay the results in "real-time", which allows accurate testing of different methods on real data with no additional effort.

![ROS Concepts](ros_concepts.png)

Another major advantage of ROS is that it provides access as a Distributed Network. Each and any node can be ran on different hardware, but will still communicate as if it were all on the same computer, if the network is properly configured. For this reason, a navigation controller and user interface can be ran on a GCS, while the flight control software is ran on-board with the image processing, while the images are post-processed on a second GCS and made available over a web interface. This drastically simplifies the work needed to write interfaces such as sockets and data streams to pass information between the separate systems.

{% endblock %}
