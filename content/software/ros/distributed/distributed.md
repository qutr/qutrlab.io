{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# Distributed ROS
A key idea behind ROS is that it always runs assuming it was on a distributed network. When this is not the case, all information in ROS points towards "localhost". What this means is that when we want to run ROS over a network using multiple systems, all we need to do is change some of these assumptions.

There are 2 main environment variables (ones that you set through the terminal/bash/ssh/etc.) that must be set:
* **ROS_MASTER_URI**: information on where the ROS master is located, and how it should be contacted.
* **ROS_IP**: Information on the device that is sending data (and where to connect to for other nodes).

In short, these are the commands needed for each variable. They will need to be set for each and every terminal session that is opened (as the environment is flushed on each new session). For example, the addresses "192.168.1.XXX" and "192.168.1.YYY" refer to the master and slave addresses respectively.

This variable need to be set on the master:
```
export ROS_IP=192.168.1.XXX
```
These variables need to be set on the slave:
```
export ROS_IP=192.168.1.YYY
export ROS_MASTER_URI=http://192.168.1.XXX:11311
```

## Common Issues
Here are some common issues you may face when trying to configure and use Distributed ROS. Here we use "node_A" and "node_B" as an example of 2 nodes running on separate PCs. We assume that "node_A" is working as expected, with issues arising when trying to run "node_B".

### "ERROR: Unable to communicate with master!" when running "node_B":
* Most likely the ROS_MASTER_URI has not been correctly set
* Ensure you typed "http" and not "https"
* Make sure you have added the port (:11311) to the end
* If you are using a hostname, make sure you can 'ping', test using IP address instead.

### "Couldn't find an AF_INET address for ..." error message in "node_A":
* Most likely "node_B" that is publishing or subscribing to "node_A" has not configured ROS_IP correctly
* Make sure ROS_IP matches the IP address of the computer the "node_B" is running on with: `echo $ROS_IP`
* If you are running nodes in a Virtual Machine, you may have to use "Bridged Networking" for ROS distributed to work correctly.

### "Couldn't find an AF_INET address for ..." error message in "node_B":
* If your environment doesn't support using a hostname (i.e. you can't ping the other PC just using it's hostname), then you will have to set the do the do the following on each PC: `export ROS_HOSTNAME="$(hostname -I)"`

## Automation
To save some time when using a device that always connects to a separate ROS master, you could add the following commands to the ".bashrc" file. The ROS_IP business will automatically fill in the variable with your IP address (as long as you have a simple connection, and the device is connected when the terminal is opened), and you'll need to replace the IP address in the ROS_MASTER_URI.
```bash
export ROS_IP=$(hostname -I)
export ROS_MASTER_URI=http://192.168.1.YYY:11311
```

Furthermore, for a device that sometimes needs to be set up with Distributed ROS, you could add a bash function to your ".bashrc" file. This will allow you to run the command "kinetic" to automatically configure the ROS master. Running "kinetic" without any arguments will configure the terminal to be set up as the ROS master. Running the command "kinetic 192.168.1.YYY" will configure the terminal to connect to a ROS master on the IP: 192.168.1.YYY.
```bash
kinetic() {
  # Setup for distributed ROS
  export ROS_IP="$(hostname -I)"
  echo "Identifying as: $ROS_IP"

  if [ "$#" -eq 1 ]
  then
    export ROS_MASTER_URI="http://$1:11311"
    echo "Connecting to: $ROS_MASTER_URI"
  fi
}
```

{% endblock %}
