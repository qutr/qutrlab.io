{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

## Setting up the LAMP Stack
In Ubuntu 16.04, you will need to install some dependencies before you can start doing any web integration:
```sh
sudo apt install apache2 mysql-server python-mysql.connector
sudo apt install php7.0 libapache2-mod-php7.0
```
During the installation, you will be asked to setup an admin user account. For the purposes of this example, the username was chosen to be `root`, with the password `mysql`.

After the installation completes, run the following command to check if PHP is running correctly:
```sh
php -r 'echo "\n\nYour PHP installation is working fine.\n\n\n";'
```
You should see a message saying `Your PHP installation is working fine.` in return.

Additionally, to check Apache is working correctly, browse to `localhost` in your we browser, where you should see the "Apache2 Ubuntu Default Page" page.

## Configuring MySQL
As a precursor, the following was mainly pulled from the [MySQL Docs Tutorials](https://dev.mysql.com/doc/refman/5.7/en/tutorial.html).

#### Creating a MySQL User Account
Before we can start using MySQL, it is best to create a secondary user, such that we can interface our programs without using the admin account.

In a terminal, run open the MySQL shell, logging in as the `root` user:
```
mysql -u root -p
```
Enter your password, and now you should be in the `mysql>` shell.

To create a new user, with username `user` and password `mysql`:
```mysql
CREATE USER 'user'@'localhost' IDENTIFIED BY 'mysql';
```

You will also have to give the new user account some privileges so it can manipulate the databases. In this case, we will grant the user full access:
```mysql
GRANT ALL PRIVILEGES ON * . * TO 'user'@'localhost';
```

Log out of the admin user:
```mysql
exit
```

#### Creating a new database and table
<!--
TODO:
This section needs to be updates to something more general
-->
In MySQL, tables are manipulated with queries. Before we can start manipulating data though, we must first create a database, and a table within that, to store our data.

First off, log into the MySQL shell (as the user we just created):
```sh
mysql -u user -p
```
We can see what databases are currently configured with the following command:
```mysql
SHOW DATABASES;
```
You'll notice that there are already a few created, but none that are good for us to use to put our data in.

To create a new database:
```mysql
CREATE DATABASE ros_db;
```
You should now be able to see it with the `show DATABASES;` command.

To actually use this database, we must first select it:
```mysql
USE ros_db;
```
We can now have a start manipulating what we will store in this database.

To create a new table:
```mysql
CREATE TABLE gas_data (timestamp DECIMAL, reading INT);
```

You can now display some descriptions of your database with:
```mysql
SHOW TABLES;
DESCRIBE gas_data;
```

There are many different data types available, and you can give a table as many columns that you wish, so have a play around with some other ideas. If you need to delete tables or databases, you can use the following:
```mysql
DROP DATABASE some_db;
DROP TABLE some_table;
```
Going forward, we will be using the `ros_db` database and the `gas_data` table, so don't `DROP` those just yet.

#### Adding data to the table
<!--
TODO:
This section needs to be updates to something more general
-->
To insert data into a table, we can use the INSERT query. You can read more about it [here](https://dev.mysql.com/doc/refman/5.7/en/loading-tables.html). For the mean time, we will look at using the `db_conn_py_node` from the
`kinetic_sample_database_connector` package (there's also a C++ example) to insert data directly from ROS.

We aren't going to look into these Nodes too closely, but they have been configured to read data from the `uavtaq_emulator`, and insert it into the `gas_data` table that we created above.

In some new terminals, launch the
```sh
# Terminal 1
roslaunch uavtaq_emulator emulator.launch

# Terminal 2
rosrun kinetic_sample_database_connector db_conn_py_node
```
You should now be receiving messages saying `Inserting new data...` in the database connector window.

> **[info] Info**
>
> Only leave the database connector running for a few (maybe 10) seconds, then close it.

Back in the MySQL window (with the `mysql>` prompt still open), you can use the following to show all the data collected in the table:
```mysql
SELECT * FROM gas_data;
```
This will select all the entries in the gas_data table, and print them to the screen.

If you want to clear out your entire table, you can run the command:
```mysql
DELETE FROM gas_data;
```

> **[info] Info**
>
> It is a good idea to make sure that you keep on top of managing the amount of data in your databases, otherwise they may grow very huge. In the worst case, you can delete entire databases to start fresh if needed.


{% endblock %}
