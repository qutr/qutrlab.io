{% extends "/CPAGE.md" %}

{% set maintainers = ["@pryre"] %}

{% block pageContent %}

# QUT Robotics

[![pipeline status](https://gitlab.com/qutr/qutr.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/qutr/qutr.gitlab.io/commits/master)

This is an open source reqpository for everything robotics at Queensland University of Technology.

{% endblock %}
