# Summary

* [Home](HOME.md)
* [About QUT Robotics](about/about.md)
  * [Joining QUTR](about/joining/joining.md)
  * [Using git](about/git/git.md)
  * [Requesting Content](about/requesting/requesting.md)
  * [Contributing to the Wiki](about/contributing/contributing.md)
  * [Code Standards](about/coding/coding.md)
  * [Example Content Page](about/example/example.md)
* Theory
  * Control
  * Navigation & Localization
  * Image Processing
  * Kinematics
  * Unmanned Ground Vehicles (UGVs)
  * Unmanned Aerial Vehicles (UAVs)
* Programming
  * Control Algorithms
  * OpenCV
  * Packet Structure
    * DIY
    * MAVLINK
* Hardware
  * Materials
  * Wiring
  * Motors
  * Batteries
  * Wheels
  * Propellers
* [Devices](devices/devices.md)
  * Software Architectures
    * ARM
    * x86
    * x64
  * Communications
    * UART
    * i2c
    * SPI
    * IP
      * Wired
      * Wireless
      * Routers & Bridges
  * Sensors
    * IMUs
    * Gyroscopes
  * Microcontrollers
    * Arduino
    * Teensy
  * [Socket on Chip (SoC)](devices/soc/soc.md)
    * Raspberry Pi
      * Raspberry Pi v2
      * Raspberry Pi v3
    * Odroid
      * Odroid U3
      * Odroid XU4
  * Full-Size Computers
* Robots
  * Baxter
* [Software](software/software.md)
  * QUTR Repositories
    * info
  * Windows
  * Macintosh
  * [Linux](software/linux/linux.md)
    * [Networking](software/linux/networking/networking.md)
    * [Remote Access](software/linux/headless/headless.md)
    * [Time Syncronization](software/linux/time/time.md)
    * [Process Priority](software/linux/priority/priority.md)
    * [USB Devices](software/linux/usb/usb.md)
  * [Robotic Operating System](software/ros/ros.md)
    * [ROS Commands & Tools](software/ros/tools/tools.md)
    * [Writing ROS Nodes](software/ros/nodes/nodes.md)
    * [Distributed ROS](software/ros/distributed/distributed.md)
    * [Sample Packages](software/ros/samples/samples.md)
    * [Examples](software/ros/examples/examples.md)
	  * Image Processing
	  * TF2
	  * Navigation
  * MATLAB
    * Simulink
  * [LAMP Web Stack](software/lamp/lamp.md)
* [Old Resources](old/old.md)
  * [Airframes](old/Airframes.md)
  * [Common Terminology](old/Common-Terminology.md)
  * [Communications](old/Communications.md)
  * [Image Processing](old/Image-Processing.md)
  * [Navigation & Localization](old/Navigation-_-Localization.md)
  * [Sensors & Estimation](old/Sensors-_-Estimation.md)
  * [Simulation](old/Simulation.md)
  * [UAV Setup Guides](old/UAV-Setup-Guides.md)
